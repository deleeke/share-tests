package BST_A2;

public class BST_Playground {
/*
 * you will test your own BST implementation in here
 *
 * we will replace this with our own when grading, and will
 * do what you should do in here... create BST objects,
 * put data into them, take data out, look for values stored
 * in it, checking size and height, and looking at the BST_Nodes
 * to see if they are all linked up correctly for a BST
 * 
*/
  
  public static void main(String[]args){

   // you should test your BST implementation in here
   // it is up to you to test it rthoroughly and make sure
   // the methods behave as requested above in the interface
  
   // do not simple depend on the oracle test we will give
   // use the oracle tests as a way of checking AFTER you have done
   // your own testing

   // one thing you might find useful for debugging is a print tree method
   // feel free to use the printLevelOrder method to verify your trees manually
   // or write one you like better
   // you may wish to print not only the node value, and indicators of what
   // nodes are the left and right subtree roots,
   // but also which node is the parent of the current node
	  
	  BST test_remove = new BST();
	  test_remove.insert("m");
	  test_remove.insert("z");
	  test_remove.insert("a");
	  test_remove.insert("AA");
	  test_remove.insert("n");
	  test_remove.printTree();
	  test_remove.insert("m");
	  test_remove.insert("z");
	  test_remove.insert("a");
	  test_remove.insert("AA");
	  test_remove.insert("n");
	  test_remove.printTree();
	  System.out.println("remove z");
	  test_remove.remove("z");
	  test_remove.printTree();
	  System.out.println("remove a");
	  test_remove.remove("a");
	  test_remove.printTree();
	  test_remove.remove("A");
	  test_remove.remove("zzzz");
	  
	  
	  BST numbers = new BST();
	  numbers.insert("oompah");
	  numbers.insert("grapples");
	  numbers.insert("loony");
	  numbers.insert("grandma");
	  numbers.insert("squid");
	  numbers.insert("oxen");
	  numbers.insert("jelly donut");
	  numbers.insert("one");
	  numbers.insert("lampshade");
	  numbers.insert("yak");
	  numbers.insert("turgid");
	  numbers.insert("apples");
	  numbers.insert("zebra");
	  numbers.printTree();
	  numbers.insert("oompah");
	  numbers.insert("zebra");
	  numbers.insert("grapples");
	  numbers.insert("loony");
	  numbers.insert("grandma");
	  numbers.insert("squid");
	  numbers.insert("jelly donut");
	  numbers.insert("one");
	  numbers.insert("lampshade");
	  numbers.insert("yak");
	  numbers.insert("apples");
	  numbers.printTree();
	  System.out.println("Minimum value is " + (numbers.empty() ? "None" : numbers.findMin()));
	  System.out.println("Maximum value is " + (numbers.empty() ? "None" : numbers.findMax()));
	  System.out.println("Size is : " + numbers.size());
	  System.out.println("Height is: " + numbers.height());
	  System.out.println("");
      System.out.println("Remove squid");
      numbers.remove("squid");
      numbers.printTree();
      System.out.println("Minimum value is " + (numbers.empty() ? "None" : numbers.findMin()));
	  System.out.println("Maximum value is " + (numbers.empty() ? "None" : numbers.findMax()));
	  System.out.println("Size is : " + numbers.size());
	  System.out.println("Height is: " + numbers.height());
	  System.out.println("");
	  System.out.println("Remove oompah");
      numbers.remove("oompah");
      numbers.printTree();
      System.out.println("Minimum value is " + (numbers.empty() ? "None" : numbers.findMin()));
	  System.out.println("Maximum value is " + (numbers.empty() ? "None" : numbers.findMax()));
	  System.out.println("Size is : " + numbers.size());
	  System.out.println("Height is: " + numbers.height());
	  System.out.println("");
	  System.out.println("Remove jelly donut");
      numbers.remove("jelly donut");
      numbers.printTree();
      System.out.println("Minimum value is " + (numbers.empty() ? "None" : numbers.findMin()));
	  System.out.println("Maximum value is " + (numbers.empty() ? "None" : numbers.findMax()));
	  System.out.println("Size is : " + numbers.size());
	  System.out.println("Height is: " + numbers.height());
	  System.out.println("");
	  System.out.println("Remove loony");
      numbers.remove("loony");
      numbers.printTree();
      System.out.println("Minimum value is " + (numbers.empty() ? "None" : numbers.findMin()));
	  System.out.println("Maximum value is " + (numbers.empty() ? "None" : numbers.findMax()));
	  System.out.println("Size is : " + numbers.size());
	  System.out.println("Height is: " + numbers.height());
	  System.out.println("");
      System.out.println("Remove zebra");
      numbers.remove("zebra");
      numbers.printTree();
      System.out.println("Minimum value is " + (numbers.empty() ? "None" : numbers.findMin()));
	  System.out.println("Maximum value is " + (numbers.empty() ? "None" : numbers.findMax()));
	  System.out.println("Size is : " + numbers.size());
	  System.out.println("Height is: " + numbers.height());
	  System.out.println("");
	  System.out.println("Remove lampshade");
      numbers.remove("lampshade");
      numbers.printTree();
      System.out.println("Minimum value is " + (numbers.empty() ? "None" : numbers.findMin()));
	  System.out.println("Maximum value is " + (numbers.empty() ? "None" : numbers.findMax()));
	  System.out.println("Size is : " + numbers.size());
	  System.out.println("Height is: " + numbers.height());
	  System.out.println("");
	  System.out.println("Remove grapples");
      numbers.remove("grapples");
      numbers.printTree();
      System.out.println("Minimum value is " + (numbers.empty() ? "None" : numbers.findMin()));
	  System.out.println("Maximum value is " + (numbers.empty() ? "None" : numbers.findMax()));
	  System.out.println("Size is : " + numbers.size());
	  System.out.println("Height is: " + numbers.height());
	  System.out.println("");
	  System.out.println("Remove yak");
      numbers.remove("yak");
      numbers.printTree();
      System.out.println("Minimum value is " + (numbers.empty() ? "None" : numbers.findMin()));
	  System.out.println("Maximum value is " + (numbers.empty() ? "None" : numbers.findMax()));
	  System.out.println("Size is : " + numbers.size());
	  System.out.println("Height is: " + numbers.height());
	  System.out.println("");
	  System.out.println("Remove squid");
      numbers.remove("squid");
      numbers.printTree();
      System.out.println("Minimum value is " + (numbers.empty() ? "None" : numbers.findMin()));
	  System.out.println("Maximum value is " + (numbers.empty() ? "None" : numbers.findMax()));
	  System.out.println("Size is : " + numbers.size());
	  System.out.println("Height is: " + numbers.height());
	  System.out.println("");
	  System.out.println("Remove one");
      numbers.remove("one");
      numbers.printTree();
      System.out.println("Minimum value is " + (numbers.empty() ? "None" : numbers.findMin()));
	  System.out.println("Maximum value is " + (numbers.empty() ? "None" : numbers.findMax()));
	  System.out.println("Size is : " + numbers.size());
	  System.out.println("Height is: " + numbers.height());
	  System.out.println("");
      System.out.println("Remove apples");
      numbers.remove("apples");
      numbers.printTree();
      System.out.println("Minimum value is " + (numbers.empty() ? "None" : numbers.findMin()));
	  System.out.println("Maximum value is " + (numbers.empty() ? "None" : numbers.findMax()));
	  System.out.println("Size is : " + numbers.size());
	  System.out.println("Height is: " + numbers.height());

  }

  static void printLevelOrder(BST tree){ 
  //will print your current tree in Level-Order...
  //https://en.wikipedia.org/wiki/Tree_traversal
    int h=tree.getRoot().getHeight();
    System.out.println("Tree is of height: " + h);
    for(int i=0;i<=h;i++){
      printGivenLevel(tree.getRoot(), i);
    }
    
  }
  static void printGivenLevel(BST_Node root,int level){
    if(root==null)return;
    if(level==0)System.out.print(root.data+" \n");
    else if(level>0){
      printGivenLevel(root.left,level-1);
      printGivenLevel(root.right,level-1);
    }
  }
}