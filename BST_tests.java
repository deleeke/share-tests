package BST_A2;

import static org.junit.Assert.*;

import java.nio.charset.Charset;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;
/*

ADD THESE METHODS TO YOUR BST_NODE to make the printing work
	public StringBuilder toString(StringBuilder prefix, boolean isTail, StringBuilder sb) {
	    if(this.hasRight()) {
	        right.toString(new StringBuilder().append(prefix).append(isTail ? "│   " : "    "), false, sb);
	    }
	    sb.append(prefix).append(isTail ? "└── " : "┌── ").append(data.toString()).append("\n");
	    if(this.hasLeft()) {
	        left.toString(new StringBuilder().append(prefix).append(isTail ? "    " : "│   "), true, sb);
	    }
	    return sb;
	}

	@Override
	public String toString() {
	    return this.toString(new StringBuilder(), true, new StringBuilder()).toString();
	}
	
*/
public class BST_tests {

	@Before
	public void setUp() throws Exception {
	}

	// weird bug where i'm getting hung up when inserting small strings and get rejected a bunch
	// because of duplicates
    //NOTE max number of nodes of a tree with height H is 2^(H+1) - 1

    @Test
	public void insertTest() {
        BST numbers = new BST();
        assertTrue(numbers.insert("50"));
        assertTrue(numbers.insert("25"));
        assertTrue(numbers.insert("75"));
        assertTrue(numbers.insert("80"));
        assertTrue(numbers.insert("20"));
        assertTrue(numbers.insert("35"));
        assertTrue(numbers.insert("60"));
        assertTrue(numbers.insert("1"));
        assertTrue(numbers.insert("40"));
        assertTrue(numbers.insert("76"));
        assertTrue(numbers.insert("85"));
	}

    @Test
    public void removeTest(){
        BST numbers = new BST();
        assertTrue(numbers.insert("50"));
        assertTrue(numbers.insert("25"));
        assertTrue(numbers.insert("75"));
        assertTrue(numbers.insert("80"));
        assertTrue(numbers.insert("20"));
        assertTrue(numbers.insert("35"));
        assertTrue(numbers.insert("60"));
        assertTrue(numbers.insert("1"));
        assertTrue(numbers.insert("40"));
        assertTrue(numbers.insert("76"));
        assertTrue(numbers.insert("85"));
        assertTrue(numbers.remove("50"));
        assertTrue(numbers.remove("25"));
        assertTrue(numbers.remove("75"));
        assertTrue(numbers.remove("80"));
        assertTrue(numbers.remove("20"));
        assertTrue(numbers.remove("35"));
        assertTrue(numbers.remove("60"));
        assertTrue(numbers.remove("1"));
        assertTrue(numbers.remove("40"));
        assertTrue(numbers.remove("76"));
        assertTrue(numbers.remove("85"));
    }
    
    @Test
    public void removeRootTest(){
        BST numbers = new BST();
        assertTrue(numbers.insert("50"));
        assertTrue(numbers.insert("25"));
        assertTrue(numbers.insert("75"));
        assertTrue(numbers.insert("80"));
        assertTrue(numbers.insert("20"));
        assertTrue(numbers.insert("35"));
        assertTrue(numbers.insert("60"));
        assertTrue(numbers.insert("1"));
        assertTrue(numbers.insert("40"));
        assertTrue(numbers.insert("76"));
        assertTrue(numbers.insert("85"));
        assertTrue(numbers.remove("50"));
        assertTrue(numbers.root.data.compareTo("75") == 0);
        assertTrue(numbers.remove("25"));
        assertTrue(numbers.remove("75"));
        assertTrue(numbers.root.data.compareTo("80") == 0);
        assertTrue(numbers.remove("80"));
        assertTrue(numbers.root.data.compareTo("85") == 0);
        assertTrue(numbers.remove("20"));
        assertTrue(numbers.remove("35"));
        assertTrue(numbers.remove("60"));
        assertTrue(numbers.remove("1"));
        assertTrue(numbers.remove("40"));
        assertTrue(numbers.remove("76"));
        assertTrue(numbers.remove("85"));
    }

    //NOTE max number of nodes of a tree with height H is 2^(H+1) - 1
    static int log(int x, int base)
    {
        return (int) (Math.log(x) / Math.log(base));
    }
    
    @Test
    public void makeBigTree(){
        BST alpha_tree = new BST();
        int NUM_CHILDREN = 1000;
        while (alpha_tree.size() < NUM_CHILDREN){
        	String random1 = getRandomString(5);
        	if (!alpha_tree.contains(random1 + "")){
        		assertTrue(alpha_tree.insert(random1 + ""));
        	}
        }
        int H = alpha_tree.height();
        assertTrue(alpha_tree.size() <= (Math.pow(2, (H+1))-1));
        assertTrue(H <= (10*log(NUM_CHILDREN +1, 2) - 1));   
    }
    
    @Test
    public void makeHugeTree(){
        BST alpha_tree = new BST();
        int NUM_CHILDREN = 400000;
        while (alpha_tree.size() < NUM_CHILDREN){
        	String random1 = getRandomString(5);
        	if (!alpha_tree.contains(random1 + "")){
        		assertTrue(alpha_tree.insert(random1 + ""));
        	}
        }
        int H = alpha_tree.height();
        assertTrue(alpha_tree.size() <= (Math.pow(2, (H+1))-1));
        assertTrue(H <= (10*log(NUM_CHILDREN +1, 2) - 1));   
    }
    
    @Test
    public void makeAndDestroySmallAlphaTree(){
        BST alpha_tree = new BST();
        for (int i=0; i < 30; i++){
            String random1, random2;
        	random1 = getRandomString(2);
        	if (!alpha_tree.contains(random1)){
        		assertTrue(alpha_tree.insert(random1));
        	}
        	random2 = getRandomString(2);
        	boolean result = alpha_tree.remove(random2);
        	if (!result && alpha_tree.contains(random2)){
        		System.out.println(random2 + " not removed");
       			alpha_tree.printTree();
       		}
        }
        alpha_tree.insert("A");
        alpha_tree.insert("zzzzzzzzz");
        assertTrue(alpha_tree.findMax().compareTo("zzzzzzzzz") == 0);
        assertTrue(alpha_tree.findMin().compareTo("A") == 0);
		alpha_tree.printTree();
        
        int H = alpha_tree.height();
        assertTrue(alpha_tree.size() <= (Math.pow(2, (H+1))-1));
    }
    
    @Test
    public void makeAndDestroyTreeFromLimitedInput(){
        BST alpha_tree = new BST();
        for (int i=0; i < 500000; i++){
            String random1, random2;
        	random1 = getStringFromList();
        	alpha_tree.insert(random1 + "");
        	random2 = getStringFromList();
        	alpha_tree.remove(random2+"");
        }
        alpha_tree.insert("A");
        alpha_tree.insert("zzzzzzzzz");
        assertTrue(alpha_tree.findMax().compareTo("zzzzzzzzz") == 0);
        assertTrue(alpha_tree.findMin().compareTo("A") == 0);
		alpha_tree.printTree();
        
        int H = alpha_tree.height();
        assertTrue(alpha_tree.size() <= (Math.pow(2, (H+1))-1));
    }
    
    @Test
    public void makeAndDestroyBigAlphaTree(){
        BST alpha_tree = new BST();
        for (int i=0; i < 200000; i++){
            String random1, random2;
        	random1 = getRandomString(2);
        	if (!alpha_tree.contains(random1 + "")){
        		assertTrue(alpha_tree.insert(random1 + ""));
        	}
        	random2 = getRandomString(2);
        	if (alpha_tree.contains(random2+"")){
        		assertTrue(alpha_tree.remove(random2+""));
        	}
        }
        
        int H = alpha_tree.height();
        assertTrue(alpha_tree.size() <= (Math.pow(2, (H+1))-1));
        alpha_tree.insert("A");
        assertTrue(alpha_tree.findMin().compareTo("A") == 0);
    }
    
    protected String getRandomString(int len) {
        String RANDCHARS = "abcdefghijklmnopqrstuvwxyz";
        StringBuilder result = new StringBuilder();
        Random rnd = new Random();
        while (result.length() < len) {
            int index = (int) (rnd.nextFloat() * RANDCHARS.length());
            result.append(RANDCHARS.charAt(index));
        }
        return result.toString();

    }
    
    protected String getStringFromList(){
    	String words[] = {"cow", "mastiff", "a", "xena", "turgid", "superman", 
    						"wow", "no", "oompah", "grapples", "loony", 
    						"grandma", "squid", "oxen", "jelly donut", "one",
    						"lampshade", "yak", "yatzee!", "apropos", "regular" };
    	int random_indx = (int )(Math.random() * (words.length - 1));
    	return words[random_indx];
    }
    
    
}
